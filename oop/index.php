<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        require('animal.php');
        require('frog.php');
        require('ape.php');

        $sheep = new Animal("shaun");

        echo $sheep->name; // "shaun"
        echo "<br>";
        echo $sheep->legs; // 4
        echo "<br>";
        echo $sheep->cold_blooded; // "no"
        echo "<br>";

        $sungokong = new Ape("kera sakti");
        $sungokong->yell(); // "Auooo"

        $frogs = new Frog("buduk");
        $frogs->jump() ; // "hop hop"

        

    
    ?>
</body>
</html>